from os.path import abspath
import IPython

from avatar2 import *

def main():

    # Configure the location of various files
    firmware = abspath('./blink.elf')

    openocd_config = abspath('./pico-picoprobe.cfg')

    # Initiate the avatar-object
    avatar = Avatar(arch=ARM_CORTEX_M3, output_directory='/tmp/avatar')

    # Create the target-objects
    pico = avatar.add_target(OpenOCDTarget, openocd_script=openocd_config)

    # Create controlled qemu instance
    qemu = avatar.add_target(QemuTarget, gdb_port=1236)

    rom  = avatar.add_memory_range(0x00000000, 0x00004000, permissions='rx', name='ROM') # 16kB ROM with bootloader
    # The flash is accessed using a XIP with caching interface, different address spaces map to different caching strategies
    # If disabled the XIP cache (16kB) can be used as additional SRAM
    xip_cached              = avatar.add_memory_range(0x10000000, 0x00200000, name='Flash (XIP) - cache') # 2MB Flash, Accessing using XIP with cache
    xip_no_cache_update     = avatar.add_memory_range(0x11000000, 0x00200000, name='Flash (XIP) - no cache update but lookup') # 2MB Flash, Accessing using XIP with cache but never update cache
    xip_no_hit_cache_update = avatar.add_memory_range(0x12000000, 0x00200000, name='Flash (XIP) - cache updates but no lookup') # 2MB Flash, Accessing using XIP with cache but without hit check -> always updates cache
    xip_no_cache            = avatar.add_memory_range(0x13000000, 0x00200000, name='Flash (XIP) - no cache') # 2MB Flash, Accessing using XIP without cache
    # Not syncing the cache should only have an impact on performance but not on correctness
    # xip  = avatar.add_memory_range(0x15000000, 0x00004000) # 16kB XIP cache ONLY READABLE if CTRL_EN register is set to disable the XIP caching, in which case this can be used as additional SRAM by application
    ram             = avatar.add_memory_range(0x20000000, 0x00042000, name='RAM') # 264kB of RAM
    apb_peripherals = avatar.add_memory_range(0x40000000, 0x0006c000, forwarded=True, forwarded_to=pico) # ABP peripherals, system and control registers
    dma_regs = avatar.add_memory_range(0x50000000, 0x00000ac8, forwarded=True, forwarded_to=pico) # AHB-Lite peripherals
    usb = avatar.add_memory_range(0x50100000, 0x0001000, forwarded=True, forwarded_to=pico) # General USB range 0x0-0xff are used for control registers,  0x100-0xfff data buffer
    usb_control_regs = avatar.add_memory_range(0x50110000, 0x0000009c, forwarded=True, forwarded_to=pico) # USB control registers
    pio0 = avatar.add_memory_range(0x50200000, 0x00000144, forwarded=True, forwarded_to=pico) # PIO0 registers
    pio1 = avatar.add_memory_range(0x50300000, 0x00000144, forwarded=True, forwarded_to=pico) # PIO1 registers
    XIP_AUX_BASE = avatar.add_memory_range(0x50400000, 0x00000004, forwarded=True, forwarded_to=pico) # XIP_AUX_BASE which is an alias to the XIP STREAM_FIFO register for parallel data operations
    sio = avatar.add_memory_range(0xd0000000, 0x0000017c, forwarded=True, forwarded_to=pico) # SIO registers
    arm_peripherals = avatar.add_memory_range(0xe0000000, 0x00000eda4, forwarded=True, forwarded_to=pico) # Internal peripherals of the Cortex M0+


    # Initialize the targets
    avatar.init_targets()

    # Set breakpoint right before the start of the loop
    pico.set_breakpoint(0x1000036c, hardware=True)
    pico.cont()
    pico.wait()
    print("breakpoint hit")
    print("read reg from pico : ", pico.read_register("r4"))

    # Let the loop run once more
    pico.cont()
    pico.wait()

    print("breakpoint hit")
    print("read reg from pico : ", pico.read_register("r4"))


    # print("read reg from qemu before transfer", qemu.read_register("r4"))
    # Sync memory ranges from pico to qemu, sync registers, ROM, Flash (XIP) and SRAM
    # We only need to transfer one of the XIP ranges because they all point to the same flash, just with different caching strategies
    avatar.transfer_state(pico, qemu, sync_regs=True, synced_ranges=[rom, xip_no_cache, ram,
                                                                      apb_peripherals, dma_regs, 
                                                                      usb, usb_control_regs, 
                                                                      pio0, pio1, 
                                                                      XIP_AUX_BASE, 
                                                                      sio, arm_peripherals, ])
    print("Transfer successful")

    # print("read reg from qemu after transfer ",qemu.read_register("r4"))

    #put any logic needed for debugging.

    #execution of qemu currently doesnt work, more investigation into memory ranges to be synced is needed
    #qemu.cont()
    #qemu.wait()
    #qemu.set_breakpoint(0x1000037a)

    IPython.embed()
    print("================== Done =========================")


    avatar.shutdown()
    return


if __name__ == '__main__':
    main()
